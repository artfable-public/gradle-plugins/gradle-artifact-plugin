# Gradle Artifact Plugin
(version: 0.0.4)

## Overview
Simple plugin that adds two tasks for making source and javadoc jar

## Install
in _settings.gradle.kts_:
```kotlin
pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
        maven(url = "https://gitlab.com/api/v4/groups/68820060/-/packages/maven")
    }
}
```

in build script:
```kotlin
plugins {
    id("artfable.artifact") version "0.0.4"
}
```

## Usage
Call `javadocJar` or `sourceJar` task

`artifactoryCredentials.user` and `artifactoryCredentials.key` will contain values from gradle properties 
`artifactoryUser` and `artifactoryKey` or from env vars if properties aren't provided `ARTIFACTORY_USER` and `ARTIFACTORY_KEY`