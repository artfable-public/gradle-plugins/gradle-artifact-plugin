package com.artfable.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.internal.tasks.DefaultSourceSetContainer
import org.gradle.api.tasks.bundling.Jar
import org.gradle.kotlin.dsl.create

/**
 * @author artfable
 * 11.09.16
 */
open class GradleArtifactPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        val javadocTask = project.tasks.create("javadocJar", Jar::class) {
            group = "build"
            archiveClassifier.set("javadoc")
            from("build/docs/javadoc")
        }
        val sourceTask = project.tasks.create("sourceJar", Jar::class) {
            group = "build"
            archiveClassifier.set("sources")
            from((project.findProperty("sourceSets") as DefaultSourceSetContainer).getByName("main").allSource)
        }

        project.tasks.findByName("compileJava")?.let {
            sourceTask.mustRunAfter(it)
            javadocTask.mustRunAfter(it)
        }
        project.tasks.findByName("compileKotlin")?.let {
            sourceTask.mustRunAfter(it)
            javadocTask.mustRunAfter(it)
        }

        val credentials =
            project.extensions.create("artifactoryCredentials", ArtifactoryCredentialsExtension::class.java)

        credentials.user = if (project.hasProperty("artifactoryUser")) {
            project.property("artifactoryUser").toString()
        } else System.getenv("ARTIFACTORY_USER")

        credentials.key = if (project.hasProperty("artifactoryKey")) {
            project.property("artifactoryKey").toString()
        } else System.getenv("ARTIFACTORY_KEY")

    }

    open class ArtifactoryCredentialsExtension {
        var user: String? = null
        var key: String? = null
    }
}