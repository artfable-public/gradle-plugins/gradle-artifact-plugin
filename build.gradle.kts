group = "com.artfable.gradle"
version = "0.0.4"

plugins {
    `kotlin-dsl`
    `maven-publish`
}

val gitlabToken = findProperty("gitlabPersonalApiToken") as String?

repositories {
    maven {
        url = uri("https://gitlab.com/api/v4/groups/68820060/-/packages/maven")
        name = "GitLab"
        credentials(HttpHeaderCredentials::class) {
            name = "Private-Token"
            value = gitlabToken
        }
        authentication {
            create("header", HttpHeaderAuthentication::class)
        }
    }
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileJava {
        targetCompatibility = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestJava {
        targetCompatibility = "1.8"
    }
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
//            artifact(tasks["sourceJar"])
//            artifact(tasks["javadocJar"])

            pom {
                description.set("Simple plugin that adds two tasks for make source an javadoc jar")
                licenses {
                    license {
                        name.set("MIT")
                        url.set("https://gitlab.com/artfable-public/gradle-plugins/gradle-artifact-plugin/-/raw/master/LICENSE")
                        distribution.set("repo")
                    }
                }
                developers {
                    developer {
                        id.set("artfable")
                        name.set("Artem Veselov")
                        email.set("art-fable@mail.ru")
                    }
                }
            }
        }
    }

    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/46898123/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = gitlabToken
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

gradlePlugin {
    plugins {
        create("gradle-artifact-plugin") {
            id = "artfable.artifact"
            implementationClass = "com.artfable.gradle.GradleArtifactPlugin"
        }
    }
}
